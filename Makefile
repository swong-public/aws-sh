
all:
		@echo targets: clean dist upload
		@echo "   clean - removes dist directory"
		@echo "   dist - build and package into dist directory"
		@echo "   upload - upload built package in dist to PyPI"

dist: build
		python setup.py sdist

# upload to PyPI
upload:
		twine upload dist/*

clean:
	rm -rf dist MANIFEST

build:
	@echo "VERSION='$$(cat VERSION.txt)'" > aws-sh/version.py

	



