A shell for aws commands to manage AWS EC2 and S3
  Requires module aws-cmd 

install module: 
  python setup.py install

install using pip:
  pip install aws-cmd
  pip install [PATH]/aws-sh-[VERSION].tgz

Run with:
  python -m aws-sh
